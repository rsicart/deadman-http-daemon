# Deadman Http Server

HTTP Server which uses deadman_state_machine module.

This server handles 2 type of HTTP requests:

* `GET` requests for healtchecks  on path `/httprobe`
* `POST` requests for alerts/resolves

Default Deadman Switch implementation for Prometheus setups sends a
`POST` request continually, actually to /dev/null .

The objective of this project is to implement the other side of Deadman Switch:

* receive `POST` requests from Prometheus continually,
* for every `POST`, update last seen ping,
* and if ping is not updated for a period of time, send an alert to a receiver
  (your receiver of choice, configured in settings) bypassing AlertManager

In order to check last seen ping and transition to a different state if needed,
DeadmanHTTPServer overrides
[socketserver.BaseServer.service_actions()](https://docs.python.org/3/library/socketserver.html#socketserver.BaseServer.service_actions)
method.

For more details, look at deadman_state_machine package's README.md file.


## Using with Docker

To launch `deadman_http_server.py` in a Docker container, follow the steps below.

### 1. Create a `settings.py` file inside `settings` folder. You can see some examples in `settings.example.py`.

### 2. Build the image

```
docker build -t my-deadman-daemon .
```

### 3. Run a container using the image and settings (through a volume) from previous steps

```
docker run -it --rm --name my-deadman-running my-deadman-daemon
docker run -it --rm -v "$(pwd)"/settings/settings.py:/usr/src/app/settings/settings.py:ro --name my-deadman-running my-deadman-daemon
```

Or run it exposing a port and with daemon arguments:

```
docker run -it --rm -p8000:8000 -v "$(pwd)"/settings/settings.py:/usr/src/app/settings/settings.py:ro --name my-deadman-running my-deadman-daemon python deadman_http_server.py --timeout 10 --debug
```


## Override alert and resolve payloads

In `settings.RECEIVERS` you can define your list of receivers.

For each receiver, you must provide a dictionary of settings.

In those settings, you can define keys `alert_dict` or `resolve_dict`, with a
dictionary value containing the payload that will be send to your receivers.


## Override headers

In `settings.RECEIVERS` you can define your list of receivers.

For each receiver, you must provide a dictionary of settings.

In those settings, you can define keys `header_dict`, with a
dictionary value containing all HTTP headers you want to configure for your receiver.

## Create a new receiver type

For example a new receiver called `HttpGetReceiver`.

1. Create a new parser `HttpGetReceiverParser` for the receiver in
   `receiver_parser.py`.  Your parser must subclass ReceiverParser class.

2. In `settings.RECEIVERS`, add a new dictionary containing a list of
   HttpGetReceiver settings:

``` RECEIVERS = { ...  'HttpGetReceivers': [ { ... }, { ... }, ], ```

3. Extend `http_server.py` to use your new `HttpGetReceiverParser`


## Publishing a docker image in this Container Registry's repo

Based on [official documentation](https://docs.gitlab.com/ee/user/packages/container_registry/).

Create a Gitlab token with read/write access to the registry.

From your computer, login, build and push:

```
docker login registry.gitlab.com -u <username> -p <token>
docker build -t registry.gitlab.com/rsicart/deadman-http-daemon:latest .
docker tag registry.gitlab.com/rsicart/deadman-http-daemon:latest registry.gitlab.com/rsicart/deadman-http-daemon:v0.1.6
docker push registry.gitlab.com/rsicart/deadman-http-daemon:v0.1.6
docker push registry.gitlab.com/rsicart/deadman-http-daemon:latest
```
